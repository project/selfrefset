<?php

/**
 * @file
 * Helper functions for selfrefset module.
 *
 * @todo Handle language changing on save action.
 */

/**
 * @todo Better unset() for unnecassery fields and uncahnged fields.
 *       And do not call the field_attach_update() if has no change.
 *
 * @param object $entity_b
 *    Entity object before save.
 *
 * @param object $entity_a
 *    Entity object after save
 *
 * @param string $entity_type
 *    Entity type identifier.
 */
function selfrefset_synchronize($entity_b, $entity_a, $entity_type) {
  $ids = entity_extract_ids($entity_type, $entity_a);
  $entity_a_id = $ids[0];
  $entity_a_bundle = $ids[2];
  if (!$entity_a_id OR !$entity_a_bundle) {
    return;
  }
  $synchronized_fields = selfrefset_synchronized_fields($entity_type, $entity_a_bundle);
  if (empty($synchronized_fields)) {
    return;
  }

  $unchanged_fields = array();
  $entities = array();
  foreach ($synchronized_fields as $field_name) {
    $langcode = field_language($entity_type, $entity_a, $field_name);
    $field = field_info_field($field_name);
    $field_type_info = field_info_field_types($field['type']);

    // Store the items for later comparision.
    // @todo Test - Maybe the $langcode does not avalilable at insert time for $entity_b
    $field_items = array(
      'before' => isset($entity_b->{$field_name}[$langcode]) ? $entity_b->{$field_name}[$langcode] : array(),
      'after' => $entity_a->{$field_name}[$langcode],
    );

    /*
    if ($field_items['before'] == $field_items['after']) {
      $unchanged_fields[] = $field_name;
      continue;
    }
     *
     */

    $refered_entities = array(
      'before' => array(),
      'after' => array(),
    );
    foreach (array('before', 'after') as $state) {
      if (!$field_items[$state]) {
        continue;
      }

      foreach ($field_items[$state] as $item) {
        $refered = selfrefset_refered_entity($field_type_info, $item);
        // @todo Better method handling, and hooks for methods.
        switch ($field_type_info['selfrefset']['entity_type']['method']) {
          case 'fixed':
            $refered['entity_type'] = $field_type_info['selfrefset']['entity_type']['fixed'];
            break;

          case 'column':
            $column = $field_type_info['selfrefset']['entity_type']['column'];
            $refered['entity_type'] = $item[$column];
            break;
        }

        switch ($field_type_info['selfrefset']['entity_id']['method']) {
          case 'column':
            $column = $field_type_info['selfrefset']['entity_id']['column'];
            $refered['entity_id'] = $item[$column];
            break;
        }

        $key = "{$refered['entity_type']}:{$refered['entity_id']}";
        if (!$refered['entity_type'] OR !$refered['entity_id'] OR in_array($key, $refered_entities[$state])) {
          // To fetch the entity properties is faild or already processed.
          continue;
        }

        // Referenced entity in the current $field_name and the current $item.
        $entity = isset($entities[$key]) ?
          $entities[$key]
          :
          reset(entity_load($refered['entity_type'], array($refered['entity_id'])))
        ;

        if (!$entity) {
          // The referenced entity does not exists.
          continue;
        }

        $ids = entity_extract_ids($refered['entity_type'], $entity);
        if (
          // Fetch the bundle name is faild
          empty($ids[2])
          OR
          // Field $field_name does not exists in the referenced bundle.
          !isset($field['bundles'][$refered['entity_type']])
          OR
          !in_array($ids[2], $field['bundles'][$refered['entity_type']])
          OR
          // Skip the current entity.
          $key == "$entity_type:$entity_a_id"
        ) {
          continue;
        }

        $sf = selfrefset_synchronized_fields($refered['entity_type'], $ids[2]);
        if ($entity_type != $refered['entity_type'] OR $entity_a_bundle != $ids[2]) {
          // Compute synchronized field intersection between two different entity_type:bundle
          $sf = array_intersect_key($synchronized_fields, $sf);
        }
        // Get all field instances of the refered entity
        $instances = field_info_instances($refered['entity_type'], $ids[2]);
        $not_sf = array_diff_key($instances, $sf);
        foreach (array_keys($not_sf) as $fn) {
          // Remove the unnecassery fields.
          //unset($entity->{$fn});
        }
        $refered_entities[$state][$key] = $entity;
        $entities[$key] = $entity;
      }
    }

    $processed = array();
    foreach ($refered_entities['before'] as $key => $entity) {
      $processed[$key] = $key;

      if (!isset($refered_entities['after'][$key])) {
        // deleted reference
        $entity->{$field_name} = array();
      }
      else {
        $entity->{$field_name} = $entity_a->{$field_name};
      }
    }

    foreach ($refered_entities['after'] as $key => $entity) {
      if (isset($processed[$key])) {
        continue;
      }
      $processed[$key] = $key;
      $entity->{$field_name} = $entity_a->{$field_name};
    }
  }

  foreach ($entities as $key => $entity) {
    $et = NULL;
    $eid = NULL;
    list($et, $eid) = explode(':', $key);

    foreach ($unchanged_fields as $field_name) {
      unset($entity->$field_name);
    }

    field_attach_update($et, $entity);
  }
}
